import { getAccessToken } from "./tokens";

/**
 * Return the cell values of the Sheets as a 2D array.
 * 
 * @param accessToken An OAuth Access Token as authorization for Google API.
 */
export async function getSheetsData(accessToken: string){
    accessToken = accessToken === '' ? await getAccessToken() : accessToken;

    const sheetId = process.env.REACT_APP_SHEETS_ID as string;
    const range = "Sheet1!A1:C1000";

    const url = `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${range}`;

    const response = await fetch(url, {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
    })

    if (response.ok){
        const json = await response.json();
        return json.values as Array<Array<string>>;
    }else{
        throw Error('There was an error retrieving data from the Sheets.');
    }
}