import { getAccessToken } from "./tokens";

/**
 * Append a row to the Sheets.
 *
 * @param row An array of strings where each element represents a cell.
 * @param accessToken An OAuth Access Token as authorization for Google API.
 */
export async function appendRowToSheets(row: Array<string>, accessToken: string) {
  accessToken = accessToken === '' ? await getAccessToken() : accessToken;

  const sheetId = process.env.REACT_APP_SHEETS_ID as string;
  const range = "Sheet1!A1:C1000";

  const url = `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${range}:append`;
  const valueRange = {
    range: range,
    majorDimension: "ROWS",
    values: [row],
  };

  const params = {
    valueInputOption: "RAW",
    insertDataOption: "INSERT_ROWS",
  };

  const response = await fetch(url + "?" + new URLSearchParams(params), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    },
    body: JSON.stringify(valueRange),
  });

  if (!response.ok){
    throw Error('Error appending row to Sheets.')
  }
}
