import rs from "jsrsasign";

interface ServiceAccountKey {
  token_uri: string;
  private_key: string;
  client_email: string;
}

/**
 * Generate a JWT (JSON Web Token) using a Google Service Account Key.
 * Based on tutorials: https://github.com/kjur/jsrsasign/wiki/Tutorial-for-JWT-generation
 * https://stackoverflow.com/questions/28751995/how-to-obtain-google-service-account-access-token-javascript
 * https://developers.google.com/identity/protocols/oauth2/service-account#httprest
 *
 * @param secret The Service Account Secret Key
 */
function generateJWT(secret: ServiceAccountKey) {
  const payload = {
    aud: secret.token_uri, // Audience
    scope: "https://www.googleapis.com/auth/spreadsheets",
    iss: secret.client_email, // Issuer
    exp: Math.floor(Date.now() / 1000) + 3600, // Expiration Time
    iat: Math.floor(Date.now() / 1000), // Issue At
  };

  const header = {
    alg: "RS256",
    typ: "JWT",
  };

  const sJWT = rs.KJUR.jws.JWS.sign(
    "RS256",
    header,
    payload,
    secret.private_key
  );

  return sJWT;
}

export async function getAccessToken() {
  let private_key = process.env.REACT_APP_PRIVATE_KEY as string;
  // Change escaped new line characters (caused by env) to normal new line characters
  private_key = private_key.replace(/\\n/g, '\n');

  const secret = {
    token_uri: process.env.REACT_APP_TOKEN_URI,
    private_key: private_key,
    client_email: process.env.REACT_APP_CLIENT_EMAIL,
  } as ServiceAccountKey;
  const sJWT = generateJWT(secret);

  const encodedPayload = [
    encodeURIComponent("grant_type") +
      "=" +
      encodeURIComponent("urn:ietf:params:oauth:grant-type:jwt-bearer"),
    encodeURIComponent("assertion") + "=" + encodeURIComponent(sJWT),
  ];

  // Join the params to a string and replace spaces with +
  const body = encodedPayload.join("&").replace(/%20/g, "+");

  const response = await fetch(secret.token_uri, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: body,
  });

  if (response.ok) {
    const json = await response.json();
    return json["access_token"];
  } else {
    throw Error("Could not get an Access Token!");
  }
}
