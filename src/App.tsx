import { useEffect, useState } from "react";
import "./App.css";
import AppendDataForm from "./components/AppendDataForm";
import DisplaySheetsData from "./components/DisplaySheetsData";
import { getAccessToken } from "./google_api";

function App() {
  const [accessToken, setAccessToken] = useState("");

  useEffect(() => {
    function refreshAccessToken() {
      getAccessToken().then((token) => {
        setAccessToken(token);
        // Refresh the access token every 50 mins (token expires in 1hr)
        setTimeout(refreshAccessToken, 3000000);
      });
    }

    refreshAccessToken();
  }, []);

  return (
    <div>
      <AppendDataForm accessToken={accessToken}></AppendDataForm>
      <DisplaySheetsData accessToken={accessToken}></DisplaySheetsData>
    </div>
  );
}

export default App;
