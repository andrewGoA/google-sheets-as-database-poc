import { FormEvent } from "react";
import { appendRowToSheets } from "../google_api";

interface FormElements extends HTMLCollection {
    first: HTMLInputElement,
    last: HTMLInputElement,
    email: HTMLInputElement
}

export default function AppendDataForm (props: {accessToken: string}) {
    function onSubmit(e: FormEvent){
        // Prevent the sending of the default form request
        e.preventDefault();

        // Retrieve the data entered in the form inputs
        const elements = (e.target as HTMLFormElement).elements as FormElements;

        const firstName = elements.first.value;
        const lastName = elements.last.value;
        const email = elements.email.value;

        // Append the data to the google sheets
        appendRowToSheets([firstName, lastName, email], props.accessToken);
    }

    return <form onSubmit={onSubmit}>
        <label>First Name: </label><br/>
        <input type='text' name='first'/><br/>
        <label>Last Name: </label><br/>
        <input type='text' name='last'/><br/>
        <label>Email: </label><br/>
        <input type='text' name='email'/><br/>
        <input type="submit"/><br/>
    </form>
}