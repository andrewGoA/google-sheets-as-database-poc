import { useState } from "react";
import { getSheetsData } from "../google_api";

export default function DisplaySheetsData(props: { accessToken: string }) {
  // Retrieve data from API

  const [data, setData] = useState([] as string[][]);

  function loadApiData() {
    getSheetsData(props.accessToken).then((sheetsData) => {
      setData(sheetsData);
    });
  }

  let rows = [];
  let i = 0;

  for (let row of data) {
    let cells = [];
    let j = 0;

    for (let cell of row) {
      cells.push(<td key={j.toString()}>{cell}</td>);
      j += 1;
    }

    rows.push(<tr key={i.toString()}>{cells}</tr>);
    i += 1;
  }

  return (
    <div>
      <button onClick={loadApiData}>Load data from API</button>
      <table style={{border: '1px solid black'}}>{rows}</table>
    </div>
  );
}
