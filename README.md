## Google Sheets as Database PoC (React App)
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### ENV Variables:
```
The following env variables are required (put in .env.local or equivalent):
# ID of the Google Sheets
# This can be retrieved from a sharing link to the Google Sheets:
# https://docs.google.com/spreadsheets/d/{id}/edit?usp=sharing
REACT_APP_SHEETS_ID=1splA0G5CrXNFsU9VDoor05xJwP6P-hyCIZ9v8730_rM

# Private key for the Google Service Account
# The Private key data is retrieved from the Google Service Account "Credentials" tab where a JSON file with the required information can be downloaded

REACT_APP_PRIVATE_KEY=-----BEGIN PRIVATE KEY-----\n....\n-----END PRIVATE KEY-----\n
REACT_APP_CLIENT_EMAIL=....@....gserviceaccount.com
REACT_APP_TOKEN_URI=...
```